// const foo = (bar) => bar && 42

const foo = (bar) => {
    if (bar) {
        console.log(42);
    }

    console.log('no');
}